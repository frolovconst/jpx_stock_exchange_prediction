import click
import pandas as pd


@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument('output_file', type=click.Path())
def add_cat_feature(input_file, output_file):
    input_csv = pd.read_csv(input_file)
    input_csv['gr_half'] = input_csv['base'] > .5
    input_csv.to_csv(output_file, index=False)


if __name__ == '__main__':
    add_cat_feature()