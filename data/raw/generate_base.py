import pandas as pd
import numpy as np
import click

N_ROWS = 1000


@click.command()
@click.argument('random_seed_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def generate_base_data(random_seed_path, output_path):
    with open(random_seed_path, 'r') as f:
        random_seed = int(f.read())
    np.random.seed(random_seed)
    dataset = pd.DataFrame({'base': np.random.random(N_ROWS)})
    dataset.to_csv(output_path, index=False)


if __name__ == '__main__':
    generate_base_data()
