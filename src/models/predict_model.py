import pandas as pd
import numpy as np
import json
import click

N_ROWS = 1000


@click.command()
@click.argument('dataset_path', type=click.Path(exists=True))
@click.argument('metrics_path', type=click.Path())
def calculate_metrics(dataset_path, metrics_path):
    dataset = pd.read_csv(dataset_path)
    metrics = {'product': np.dot(dataset['gr_half'], dataset['base'])}
    print(metrics)
    with open(metrics_path, 'w') as f:
        json.dump(metrics, f)


if __name__ == '__main__':
    calculate_metrics()
