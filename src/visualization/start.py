# from data.raw import generate_base_data
from data.raw import generate_base_data

RANDOM_SEED_PATH = '../../data/raw/data_random_seed.txt'
BASE_DATA_PATH = '../../dataset/base.csv'

if __name__ == '__main__':
    # data.raw.generate_base_data(RANDOM_SEED_PATH, BASE_DATA_PATH)
    generate_base_data(RANDOM_SEED_PATH, BASE_DATA_PATH)
