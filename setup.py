from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    py_modules=['hello'],
    entry_points='''
        [console_scripts]
        hello=src.expts.click_explo:cli
    ''',
    description='bright',
    author='Ivan Mutantov',
    license='MIT',
)
