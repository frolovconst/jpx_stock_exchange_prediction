#!/usr/bin/env python
# coding: utf-8

# In[12]:


import pandas as pd
import numpy as np
from tqdm.auto import tqdm
from lightgbm import LGBMRegressor
import jpx_tokyo_market_prediction


# In[3]:
df = pd.read_csv("../data/train_files/stock_prices.csv")
df.Date = pd.to_datetime(df.Date)


# In[4]:


targets = pd.pivot_table(
    df,
    values="Target",
    index="Date",
    columns="SecuritiesCode",
)
closes = pd.pivot_table(
    df,
    values="Close",
    index="Date",
    columns="SecuritiesCode",
).ffill()
opens = pd.pivot_table(
    df,
    values="Open",
    index="Date",
    columns="SecuritiesCode",
).ffill()
highs = pd.pivot_table(
    df,
    values="High",
    index="Date",
    columns="SecuritiesCode",
).ffill()
lows = pd.pivot_table(
    df,
    values="Low",
    index="Date",
    columns="SecuritiesCode",
).ffill()
volumes = pd.pivot_table(
    df,
    values="Volume",
    index="Date",
    columns="SecuritiesCode",
).ffill()


# In[5]:


def ror(window):

    return (
        pd.melt(
            (closes - closes.shift(window))
            / closes.shift(window),
            ignore_index=False,
        )
        .reset_index()
        .dropna()
        .rename(columns={"value": f"ror_{window}"})
    )


def vol(window):

    return (
        pd.melt(
            volumes.rolling(window).mean(),
            ignore_index=False,
        )
        .reset_index()
        .dropna()
        .rename(columns={"value": f"vol_{window}"})
    )


def atr(window):

    a = highs - lows
    b = abs(highs - closes.shift(1))
    c = abs(lows - closes.shift(1))
    return (
        pd.melt(
            pd.DataFrame(
                np.max([a, b, c], axis=0),
                index=a.index,
                columns=a.columns,
            )
            .rolling(window)
            .mean(),
            ignore_index=False,
        )
        .reset_index()
        .dropna()
        .rename(columns={"value": f"atr_{window}"})
    )


def atr_day(window):

    a = highs - lows

    return (
        pd.melt(
            a.rolling(window).mean(), ignore_index=False
        )
        .reset_index()
        .dropna()
        .rename(columns={"value": f"atrday_{window}"})
    )


def atr_gap(window):

    a = abs(highs - closes.shift(1))

    return (
        pd.melt(
            a.rolling(window).mean(), ignore_index=False
        )
        .reset_index()
        .dropna()
        .rename(columns={"value": f"atrgap_{window}"})
    )


def atr_hige(window):

    a = abs(lows - closes.shift(1))

    return (
        pd.melt(
            a.rolling(window).mean(), ignore_index=False
        )
        .reset_index()
        .dropna()
        .rename(columns={"value": f"atrhige_{window}"})
    )


def dev(window):
    return (
        pd.melt(
            (closes.diff() / closes.shift(1))
            .rolling(window)
            .std(),
            ignore_index=False,
        )
        .reset_index()
        .dropna()
        .rename(columns={"value": f"variation_{window}"})
    )


def HL(window):
    return (
        pd.melt(
            (
                highs.rolling(window).max()
                - lows.rolling(window).min()
            ),
            ignore_index=False,
        )
        .reset_index()
        .dropna()
        .rename(columns={"value": f"HL_{window}"})
    )


def market_impact(window):
    return (
        pd.melt(
            (closes.diff() / volumes)
            .rolling(window)
            .mean(),
            ignore_index=False,
        )
        .reset_index()
        .dropna()
        .rename(
            columns={"value": f"market_imact_{window}"}
        )
    )


# In[10]:


features = df[
    [
        "Date",
        "SecuritiesCode",
        "Close",
        "ExpectedDividend",
    ]
].fillna(0)

for func in [
    ror,
    vol,
    atr,
    atr_day,
    atr_gap,
    atr_hige,
    HL,
    market_impact,
]:
    for window in tqdm([1, 5, 10, 20, 40, 60, 100]):
        features = pd.merge(
            features,
            func(window),
            on=["Date", "SecuritiesCode"],
            how="left",
        )

for window in tqdm([2, 5, 10, 20, 40, 60, 100]):
    features = pd.merge(
        features,
        dev(window),
        on=["Date", "SecuritiesCode"],
        how="left",
    )

features["vol_diff"] = features["vol_20"].diff()
features["atr_diff"] = features["atr_20"].diff()


# In[13]:


X = features.fillna(0)
y = df["Target"]

Xtrain = (
    X.loc[(X.Date < "2020-01-01")]
    .drop("Date", axis=1)
    .astype(np.float32)
)
ytrain = y.loc[Xtrain.index]

m = LGBMRegressor()
m.fit(Xtrain, ytrain)


# In[14]:


subdf = df.loc[(df.Date > "2020-01-01")]
Xtest = (
    X.loc[(X.Date > "2020-01-01")]
    .drop("Date", axis=1)
    .astype(np.float32)
)

targets_2020plus = pd.pivot_table(
    subdf,
    index="Date",
    columns="SecuritiesCode",
    values="Target",
)
subdf["predictions"] = m.predict(Xtest)
preds = (
    pd.pivot_table(
        subdf,
        index="Date",
        columns="SecuritiesCode",
        values="predictions",
    )
    .reindex(targets_2020plus.index)
    .ffill()
    .bfill()
)


closes = (
    pd.pivot_table(
        df,
        values="Close",
        index="Date",
        columns="SecuritiesCode",
    )
    .ffill()
    .iloc[-101:]
)
opens = (
    pd.pivot_table(
        df,
        values="Open",
        index="Date",
        columns="SecuritiesCode",
    )
    .ffill()
    .iloc[-101:]
)
highs = (
    pd.pivot_table(
        df,
        values="High",
        index="Date",
        columns="SecuritiesCode",
    )
    .ffill()
    .iloc[-101:]
)
lows = (
    pd.pivot_table(
        df,
        values="Low",
        index="Date",
        columns="SecuritiesCode",
    )
    .ffill()
    .iloc[-101:]
)
volumes = (
    pd.pivot_table(
        df,
        values="Volume",
        index="Date",
        columns="SecuritiesCode",
    )
    .ffill()
    .iloc[-101:]
)

env = (
    jpx_tokyo_market_prediction.make_env()
)  # initialize the environment
iter_test = (
    env.iter_test()
)  # an iterator which loops over the test files
for (prices, _, _, _, _, sample_prediction) in iter_test:

    prices.Date = pd.to_datetime(prices.Date)
    date = prices.Date.iloc[0]

    # Update tables
    _closes = pd.pivot_table(
        prices,
        values="Close",
        columns="SecuritiesCode",
        index="Date",
    )
    _opens = pd.pivot_table(
        prices,
        values="Open",
        columns="SecuritiesCode",
        index="Date",
    )
    _highs = pd.pivot_table(
        prices,
        values="High",
        columns="SecuritiesCode",
        index="Date",
    )
    _lows = pd.pivot_table(
        prices,
        values="Low",
        columns="SecuritiesCode",
        index="Date",
    )
    _volumes = pd.pivot_table(
        prices,
        values="Volume",
        columns="SecuritiesCode",
        index="Date",
    )

    closes = (
        pd.concat([closes, _closes]).ffill().iloc[-101:]
    )
    opens = pd.concat([opens, _opens]).ffill().iloc[-101:]
    highs = pd.concat([highs, _highs]).ffill().iloc[-101:]
    lows = pd.concat([lows, _lows]).ffill().iloc[-101:]
    volumes = (
        pd.concat([volumes, _volumes]).ffill().iloc[-101:]
    )

    features = prices[
        [
            "Date",
            "SecuritiesCode",
            "Close",
            "ExpectedDividend",
        ]
    ].fillna(0)

    for func in [
        ror,
        vol,
        atr,
        atr_day,
        atr_gap,
        atr_hige,
        HL,
        market_impact,
    ]:
        for window in tqdm([1, 5, 10, 20, 40, 60, 100]):
            features = pd.merge(
                features,
                func(window),
                on=["Date", "SecuritiesCode"],
                how="left",
            )

    for window in tqdm([2, 5, 10, 20, 40, 60, 100]):
        features = pd.merge(
            features,
            dev(window),
            on=["Date", "SecuritiesCode"],
            how="left",
        )

    features["vol_diff"] = features["vol_20"].diff()
    features["atr_diff"] = features["atr_20"].diff()

    prices["pred"] = 1999 - np.argsort(
        np.argsort(
            m.predict(features.drop(["Date"], axis=1))
        )
    )
    sample_prediction["Rank"] = (
        prices.set_index("SecuritiesCode")["pred"]
        .reindex(sample_prediction["SecuritiesCode"])
        .values
    )

    env.predict(sample_prediction)
