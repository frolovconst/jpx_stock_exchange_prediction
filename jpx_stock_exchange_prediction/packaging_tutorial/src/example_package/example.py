import click
# from typing import int


@click.command()
@click.argument('number', type=int)
def add_one(number):
    print(number + 1)
    return number + 1


if __name__ == '__main__':
    add_one()